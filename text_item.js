class TextItem {
    constructor(name, paragraph=true) {
        this.name = name;
        this.counter = 1;
        this.node = document.createElement(paragraph ? "p": "span");
    }

    addText(bold = false) {
        let text = document.createElement("span");
        text.id = this.name + "-text-" + this.counter++;
        if (bold) {
            let bold = document.createElement("b");
            bold.appendChild(text);
            this.node.appendChild(bold)
        } else {
            this.node.appendChild(text);
        }
        return this;
    }

    addLineReturn() {
        this.node.appendChild(document.createElement("br"));
        return this;
    }

    translate(i18next) {
        for (let i = 1; i < this.counter; ++i) {
            const id = this.name + "-text-" + i
            document.getElementById(id).textContent = i18next.t(id);
        }
    }

    getNode() {
        return this.node;
    }
}