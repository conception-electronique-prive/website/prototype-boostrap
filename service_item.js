class ServiceItem {
    constructor(name) {
        this.name = name;
    }

    addTitle() {
        this.title = document.createElement("h2");
        this.title.id = `${this.name}-title`;
        this.title.innerText = this.title.id;
        return this;
    }

    addSummary() {
        this.summary = document.createElement("p");
        this.summary.id = `${this.name}-summary`;
        this.summary.innerText = this.summary.id;
        return this;
    }

    addImages(images) {
        this.images = images;
        this.image = document.createElement("img");
        this.image.src = images[0].src;
        this.image.alt = images[0].alt;
        this.image.className = "image img-thumbnail shadow-sm";

        if (this.images.length > 1) {
            this.image_index = 0;
            let that = this;
            this.image_timeout = function () {
                setTimeout(that.image_timeout, 5000);
                if (++that.image_index >= that.images.length) that.image_index = 0;
                that.image.src = that.images[that.image_index].src;
                that.image.alt = that.images[that.image_index].alt;
            }
            this.image_timeout();
        }
        return this;
    }

    addList(tree) {
        this.list = document.createElement("ul");
        this.list.id = `${this.name}-list`;
        // this.list.innerText = this.list.id;

        const crawlList = function (tree, parent, supId) {
            for (let i = 1; i <= tree.size; ++i) {
                const hasChildren = i in tree;
                let element = document.createElement("li");
                element.id = `${supId}-${i}`;
                element.innerText = element.id;
                parent.appendChild(element);
                if (hasChildren) {
                    let sublist = document.createElement("ul");
                    parent.appendChild(sublist);
                    crawlList(tree[i], sublist, element.id);
                }
            }
        }

        crawlList(tree, this.list, this.list.id);

        return this;
    }

    getNavItem() {
        let link = document.createElement("a");
        link.id = `nav-${this.name}`;
        link.className = "dropdown-item";
        link.href = `#${this.name}-content`;

        this.nav_item = document.createElement("li");
        this.nav_item.appendChild(link);
        this.nav_item.setAttribute("data-bs-dismiss", "offcanvas");
        return this.nav_item;
    }

    getItem() {
        let container = document.createElement("div");
        container.className = "container-fluid rounded";

        let box = document.createElement("div");
        box.id = `${this.name}-content`;
        box.className = "content";

        let layout = document.createElement("div");
        layout.className = "flex-container";

        container.appendChild(box);
        if (this.title) box.appendChild(this.title);
        if (this.summary) box.appendChild(this.summary);
        box.appendChild(layout);
        if (this.image) layout.appendChild(this.image);
        if (this.list) layout.appendChild(this.list);

        return box;
    }

    translate(i18next) {
        if (this.title) this.title.innerText = i18next.t(this.title.id);
        if (this.summary) this.summary.innerText = i18next.t(this.summary.id);
        if (this.list) {
            const translateList = function (i18next, parent) {
                for (const child of parent.children) {
                    if (child.tagName === "ul") translateList(i18next, child);
                    else child.innerText = i18next.t(child.id);
                }
            };

            translateList(i18next, this.list);
        }
        if (this.nav_item) this.nav_item.children.item(0).textContent = i18next.t(`${this.title.id}-short`);
    }
}

exports = {ServiceItem};