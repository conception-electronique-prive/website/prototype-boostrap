function tEntry(
    eId, // document element id
    tId // translation id
) {
    return {eId: eId, tId: tId == null ? eId : tId};
}

let tEntries = [
    tEntry('nav-title', 'cep-title'),
    tEntry('nav-services', 'services-title-short'),
    // tEntry('nav-about-us', 'about-us-title-short'),
    tEntry('nav-contact-us', 'contact-us-title-short'),
    tEntry('nav-lang'),
    tEntry('services-title'),
    // tEntry('about-us-summary'),
    tEntry('contact-us-title'),
    tEntry('contact-us-summary'),
    tEntry('contact-us-email'),
    tEntry('contact-us-address'),
    tEntry('contact-us-phone')
];

const services_items = [
    new ServiceItem("testbench")
        .addTitle()
        .addSummary()
        .addList({size: 6})
        .addImages([{src: "assets/img/testbench.jpg", alt: "testbench"}]),
    new ServiceItem("instruments")
        .addTitle()
        .addSummary()
        .addList({size: 9})
        .addImages([{src: "assets/img/instruments.jpg", alt: "instruments"}]),
    new ServiceItem("frasy")
        .addTitle()
        .addSummary()
        .addList({size: 5})
        .addImages([
            {src: "/assets/img/frasy_logo.svg", alt: "Logo frasy"},
            {src: "/assets/img/frasy_ui.png", alt: "UI Frasy"},
        ]),
    new ServiceItem("hardware-design")
        .addTitle()
        .addSummary()
        .addList({size: 7, 1: {size: 6}})
        .addImages([
            {src: "/assets/img/neurobed.jpg", alt: "neurobed"},
            {src: "/assets/img/spa.jpg", alt: "neurospa"},
            {src: "/assets/img/vision15_screen.jpg", alt: "Vision15"},
        ]),
    new ServiceItem("software-design").addTitle().addSummary().addList({size: 6}),
    new ServiceItem("consulting")
        .addTitle()
        .addSummary()
        .addList({size: 8})
        .addImages([{src: "/assets/img/consultation.png", alt: "consulting"}]),
    new ServiceItem("manufacturing").addTitle().addSummary().addList({size: 5})
];

const about_us = new TextItem("about-us")
    .addText().addLineReturn()
    .addText().addLineReturn()
    .addText().addLineReturn()
    .addText().addLineReturn()
    .addText().addLineReturn()
    .addText().addLineReturn();
document.getElementById('home-container').appendChild(about_us.getNode());

for (const item of services_items) {
    document.getElementById('nav-services-items').appendChild(item.getNavItem());
    document.getElementById('services-content').appendChild(item.getItem());
}

function updateLanguage() {
    // Change page content
    about_us.translate(i18next);
    for (const entry of tEntries) document.getElementById(entry.eId).innerText = i18next.t(entry.tId);
    for (const item of services_items) item.translate(i18next);
}

document.getElementById('nav-lang-fr').onclick = function () {
    i18next.changeLanguage('fr');
};
document.getElementById('nav-lang-en').onclick = function () {
    i18next.changeLanguage('en');
};

i18next
    .use(i18nextHttpBackend)
    .use(i18nextBrowserLanguageDetector)
    .init({debug: true, fallbackLng: 'en'}, (err, t) => {
        if (err) return console.log('something went wrong loading', err);
        t('key');
        updateLanguage();
    });
i18next.on('languageChanged', updateLanguage);
i18next.on('onLoaded', updateLanguage);